
<?php include("includes/header.php"); ?>
<?php
ob_start();
session_start();
require_once 'BDD.php';

if( !isset($_SESSION['user']) ) {
    header("Location: login.php");
    exit;
}
$ecole = $_POST['ecole'];

if (empty($ecole))
{
    print("Veuillez entrer votre école");
    exit();


}
$query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE id_uti=".$_SESSION['user']);
$query->execute();
$userinfo = $query->fetch();


if (isset($_POST['btn-mail'])) {

$email = trim($_POST['email']);
$email = strip_tags($email);
$email = htmlspecialchars($email);

$query_mail = $bdd->prepare("UPDATE UTILISATEUR SET email='$email' WHERE id_uti=".$_SESSION['user']);
$exec = $query_mail->execute();
if ($exec) {
    $error = true;
    $errTyp = "Bravo";
    $errMSG = "Votre école a bien été modifié";
    ?>
    <script type="text/javascript">
        redirectTime = "2000";
        redirectURL = "profil.php";
        setTimeout("location.href = redirectURL;",redirectTime);
    </script>
    <?php
    }
}
?>



        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Modification de votre école</h1>
                        <p>Vous pouvez modifier votre école ici: &nbsp;</p>
                    </div>
                </div>
                <div class="container">
                    <center><div class="rendez-vous">

			<div class="details">
               <form action="" method="post">
                <div class="campus">

                    <strong>Ecole:&nbsp;</strong><select>
                        <option value="3A">3A</option>
                        <option value="CBIO">CBIO</option>
                        <option value="CEFAM">CEFAM</option>
                        <option value="CFA">CFA</option>
                        <option value="CIEFA">CIEFA</option>
                        <option value="EPSI">EPSI</option>
                        <option value="ES/IL">ES/IL</option>
                        <option value="ESAM">ESAM</option>
                        <option value="GROUPEIGS">GROUPE IGS</option>
                        <option value="ICL">ICL</option>
                        <option value="IDRAC">IDRAC</option>
                        <option value="Ileft">Ileft</option>
                        <option value="IET">IET</option>
                        <option value="IFAG">IFAG</option>
                        <option value="IGEFI">IGEFI</option>
                        <option value="IGS-RH">IGS-RH</option>
                        <option value="IMIS">IMIS</option>
                        <option value="IMSI">IMSI</option>
                        <option value="IPI">IPI</option>
                        <option value="ISCPA">ISCPA</option>
                        <option value="OSS">OSS</option>
                        <option value="SUPDECOM">SUP DE COM</option>
                        <option value="WIS">WIS</option>
            </select>
                    <input type="submit" name="btn-school" value="Valider">
                </div>
                </form>
            </div>
                        </div></center></div></div></div>

        <!-- Footer section end -->

        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
