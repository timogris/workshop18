# Projet pour le premier Workshop 2018-19 de B2 à l'EPSI
Timothée Gris, Arianna Montignac, Quentin Lapierre, Marie Lahondère

## WSB2

### Comment : 
Les étudiants vont proposer une activité privée ou publique : 
-Si elle est privée : l’organisateur sera la seul à pouvoir inviter des personnes
-Si elle publique : en plus d’inviter des personnes, des étudiants pourront demander à participer, et l’organisateur pourra accepter ou refuser.

Sur la page : on pourra trier par activités, et affiner la recherche avec une barre de recherche

Les activités ne seront pas détaillées, (uniquement heure globale, vers où, nombre de participant) lorsque l’on en sélectionne une, il faudra faire partit de l’activité pour voir les détails (heure et lieu exacte, discuter avec les participants, tous les participants)

### Une activité : on pourra la supprimer, la modifier aussi bien pour la description que pour l’heure 
Activités WSB2
- Voyager (covoiturage, …) 
- Se loger (colocation, …)
- Se restaurer 
- Sortir (se divertir) 
- Se cultiver (musées, cinéma)
- Faire du sport 
- vente (vide grenier)

On pourra accéder au profil des autres : nom, photo de profil, âge, école, activités organisées

Une activité qui aura eu lieu sera automatiquement supprimée
