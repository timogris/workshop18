
<?php include("includes/header.php"); ?>
<?php

ob_start();
session_start();
require_once 'BDD.php';

// Verification si user est connecté -> sinon redirige vers login
if( !isset($_SESSION['user']) ) {
    header("Location: login.php");
    exit;
}
// Selection données de l'user
$query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE id_uti=".$_SESSION['user']);
$query->execute();
$userinfo = $query->fetch();


if (isset($_POST['btn-mail'])) {

    $email = trim($_POST['email']);
    $email = strip_tags($email);
    $email = htmlspecialchars($email);

    if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
        $error = true;
        $errorMail = "Attention";
        $errorMSG = "Veuillez entrer une adresse mail valide.";
    } else {
        // check email exist or not
        $query = $bdd->prepare("SELECT email FROM UTILISATEUR WHERE email= ?");
        $query->execute(array($email));
        $count = $query->rowCount();
        if($count!=0){
            $error = true;
            $errTyp = "Attention";
            $errMSG = "Cette adresse mail est déjà utilisée.";
        }
    }

    if(!$error) {

        $query_mail = $bdd->prepare("UPDATE UTILISATEUR SET email='$email' WHERE id_uti=" . $_SESSION['user']);
        $exec = $query_mail->execute();
        if ($exec) {
            $error = true;
            $errTyp = "Bravo";
            $errMSG = "Votre adresse mail a bien été modifié";
            ?>
            <script type="text/javascript">
                redirectTime = "2000";
                redirectURL = "profil.php";
                setTimeout("location.href = redirectURL;", redirectTime);
            </script>
            <?php
        } else {
            $error = true;
            $errTyp = "Attention";
            $errMSG = "Un problème est survenue, contactez l'administrateur";
        }
    }
}
?>
        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Modification de votre adresse mail</h1>
                        <p>Vous pouvez modifier votre adresse mail ici: &nbsp;</p>
                    </div>
                </div>
                <div class="container">
                    <center><div class="rendez vous">

			<div class="details">
               <form action="" method="post">
                <div class="mail">

                    <?php
                    if ($error) {
                        if ($errTyp == "Bravo") {
                            echo '<center><p style="color: green">'.$errMSG.'</p></center>';
                        } elseif ($errTyp == "Attention") {
                            echo '<center><p style="color: red">'.$errMSG.'</p></center>';
                        }
                    }
                    ?>

                <strong>Email:</strong><input type="email" name="email" value="" placeholder="Nouvelle adresse mail">

                </div>
                   <input type="submit" name="btn-mail" value="Valider">
                </form>
            </div>
                        </div></center></div></div></div>



        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>

