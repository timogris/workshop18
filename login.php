<!DOCTYPE html>
<html lang="en">
<?php
	ob_start();
	session_start();
	require_once 'BDD.php';

	if ( isset($_SESSION['user'])!="" ) {
			header("Location: index.php");
			exit;
	}

	$error = false;

	if( isset($_POST['btn_login']) ) {

		$mail = trim($_POST['mail']);
		$mail = strip_tags($mail);
		$mail = htmlspecialchars($mail);

		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);

		if(empty($mail)){
			$error = true;
			$mailError = "Veuillez entrer votre adresse mail.";
		} else if ( !filter_var($mail,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$mailError = "Veuillez entrer une adresse mail valide.";
		}

		if(empty($pass)){
			$error = true;
			$passError = "Veuillez entrer votre mot de passe.";
		}

		if (!$error){

			$query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE email=?");
			$query->execute(array($mail));
			$count = $query->rowCount();
			$userinfo = $query->fetch();

			if( $count == 1 && password_verify($pass, $userinfo['mdp']) ) {
				$_SESSION['user'] = $userinfo[0];
			} else {
				$errMSG = "Le mail ou le mot de passe est incorrect";
			}
			if($userinfo['verif'] == 0) {
                $errMSG = "Votre compte n'a pas été vérifié !";
            } else {
                header("Location: index.php");
			}
        }
	}
?>
    <?php include("includes/header.php"); ?>

        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Connexion</h1>

                        <p>Connectez-vous à PlansCampus. Pas encore de compte ? <a href="register.php" class="conninsc">Créez-en un par ici !</a></p>
                    </div>

                    <?php
                    if ($errMSG) {
                            echo '<center><p style="color: red">'.$errMSG.'</p></center>';
                    }
                    ?>

                </div>
                <div class="container">
                    <div class="connexion">
		      <form method="POST" action="login.php" autocomplete="off">
			<b>E-mail :</b><br />
			<input type="email" class="connexion" name="mail" placeholder="Votre Email" value="<?php echo $mail; ?>">
			<span class="text-danger"><?php echo $mailError; ?></span>
			<br />
			<b>Mot de passe :</b><br />
			<input type="password" class="connexion" name="pass" placeholder="Votre Mot de Passe" value="<?php echo $pass; ?>">
			<span class="text-danger"><?php echo $passError; ?></span>
			<br />
			<br />
			<button type="submit" class="envoyer" name="btn_login">Envoyer</button>
		      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section edn -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->

<div class="rien">

</div>

        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
<?php ob_end_flush(); ?>
