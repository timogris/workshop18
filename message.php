<!DOCTYPE html>
<?php
	ob_start();
	session_start();
    if( !isset($_SESSION['user']) ) {
		header("Location: login.php");
	}
	include_once 'BDD.php';

	$error = false;

	if ( isset($_POST['btn-contact']) ) {

		$objet = trim($_POST['objet']);
		$objet = strip_tags($objet);
		$objet = htmlspecialchars($objet);

		$msg = trim($_POST['msg']);
		$msg = strip_tags($msg);
		$msg = htmlspecialchars($msg);

		$to = $_GET['to'];
		$from = $_SESSION['user'];

		// basic name validation
		if (empty($objet)) {
			$error = true;
			$objetError = "Veuillez indiquez l'objet de votre message.";
		}

		// password validation
		if (empty($msg)){
			$error = true;
			$msgError = "Veuillez indiquez votre message.";
		} else if(strlen($msg) < 20) {
			$error = true;

		}
		// if there's no error, continue to signup
		if( !$error ) {						$res = mysql_query("SELECT * FROM personne WHERE pers_ID=".$_SESSION['user']);			$row = mysql_fetch_array($res);						$req = mysql_query("SELECT pers_mail FROM personne WHERE pers_ID=".$_GET['to']);			$rowreq = mysql_fetch_array($req);
			$to = $rowreq['pers_mail'];			$objets = "Nouveau Message";			$headers = 'MIME-Version: 1.0'."\r\n";			$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";			$headers .= 'From: "ShareBien"<noreply@sharebien.fr>'."\n";			$headers .= 'Reply-To: '.$row['pers_mail'].''."\n";			$message = file_get_contents("mailmsg.html");			$message = str_replace('{FROM_PRENOM}', $row['pers_prenom'], $message);			$message = str_replace('{FROM_NOM}', $row['pers_nom'], $message);			$message = str_replace('{OBJET}', $objet, $message);			$message = str_replace('{MSG}', $msg, $message);			$message = str_replace('{TO}', $_SESSION['user'], $message);					mail($to, utf8_encode($objets), utf8_encode($message), $headers);
			$errTyp = "Bravo";
			$errMSG = "Votre message a bien été transmis !";
		} else {
			$errTyp = "Attention";
			$errMSG = "Une erreur est survenue, si elle persiste, contactez l'équipe";
		}


	}
?>

<?php include("includes/header.php"); ?>

        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Envois de message à: </h1>
                        <p>Saisissez le message que vous souhaitez envoyer à votre destinataire</p>
                    </div>

					<?php
				if ($errMSG) {
					if ($errTyp == "Bravo") {
					echo '<center><p style="color: green">'.$errMSG.'</p></center>';
					} elseif ($errTyp == "Attention") {
					echo '<center><p style="color: red">'.$errMSG.'</p></center>';
					}
				}
			  ?>


                </div>
                <div class="container">
                    <div class="inscription">
		      <form method="post" action="message.php?to=<?php echo $_GET['to'] ?>" autocomplete="off">
			<b>Objet: </b><br />
			<input type="text" name="objet" placeholder="Objet" maxlength="50" value="<?php echo $objet ?>"/>
			<span class="text-danger"><?php echo '<br /><span style="color: red">'.$objetError.'</span>'; ?></span>
			<b>Message: </b><br />
			<textarea name="msg" placeholder="Votre message" rows=10 COLS=40/><?php echo $msg;?></textarea>
			<span class="text-danger"><?php echo '<br /><span style="color: red">'.$msgError.'</span>'; ?></span>
			<br />
			<br />
			<button type="submit" class="envoyer" name="btn-contact"><b>Envoyer</b></button>
		      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section edn -->
        <!-- Footer section start -->
        <div class="footer">
            <p>&copy; 2018 Copyrights | ShareBien</p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>

<?php ob_end_flush(); ?>
