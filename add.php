<?php include("includes/header.php"); ?>

<!-- Contact section start -->
<div id="contact" class="contact">
    <div class="section secondary-section background1">
        <div class="container">
            <div class="title">
                <h1>Créez votre évènement maintenant!</h1>
                <p> Remplissez simplement le beau formulaire juste en dessous</p>
            </div>

            <?php
            if ($errMSG) {
                if ($errTyp == "Bravo") {
                    echo '<center><p style="color: green">'.$errMSG.'</p></center>';
                } elseif ($errTyp == "Attention") {
                    echo '<center><p style="color: red">'.$errMSG.'</p></center>';
                }
            }
          ?>

          <?php

          // On vérifie que le formulaire ai été envoyé :
            if (isset($_POST['btn-signup'])) {
                // On vérifie qu'aucun champ ne soit vide (doublecheck du required) :
                if (!empty($_POST['selection']) AND !empty($_POST['nom']) AND !empty($_POST['description'])
                AND !empty($_POST['prive_public']) AND !empty($_POST['lieu']) AND !empty($_POST['date']) AND !empty($_POST['heure'])
                AND !empty($_POST['nombre_pers'])) {
                    // On récupère les informations :
                    $selection = $_POST['selection'];
                    $nom = htmlspecialchars($_POST['nom']);
                    $description = htmlspecialchars($_POST['description']);
                    $prive_public = $_POST['prive_public'];
                    $lieu = htmlspecialchars($_POST['lieu']);
                    $date = $_POST['date'];
                    $heure = $_POST['heure'];
                    $nombre_pers = $_POST['nombre_pers'];
                    // On vérifie que les listes ne sont pas vides :
                    if ($_POST['selection'] != "null") {
                        $req_inser = $bdd -> prepare("INSERT INTO ACTIVITE(type, nom, description, prive_public, lieu, date, heure, nombre_max) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                        $req_inser -> execute(array($selection, $nom, $description, $prive_public, $lieu, $date, $heure, $nombre_pers));
                        $erreur = '';

                    }
                }
            }

          ?>

        </div>
        <div class="container">
            <div class="activité">
                <form method="post" action="add.php" autocomplete="off">
                    <b> ☛ Type d'activité</b><br /><br />
                    <select id="selection" name="selection" class="type" placeholder="Type d'activité">
                        <option selected disabled>  Vous proposez...</option>
                        <option values="co-voiturage"> Un co-voiturage</option>
                        <option values="colocation"> Une colocation</option>
                        <option values="soirée"> Une grosse soirée</option>
                        <option values="cinéma"> Un cinéma</option>
                        <option values="sport"> Une activité en extérieur</option>
                    </select>
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$fnameError.'</span>'; ?></span>
                    <br />
                    <b> ✎ Nom de l'activité</b><br />
                    <input id="nom" type="text" class="activité" name="nom" placeholder="Nom de l'activité">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$nameError.'</span>'; ?></span>
                    <br />
                    <b> ☰ Description de l'activité </b><br />
                    <input id="description" type="text" class="activité" name="description" placeholder="Votre Email">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$emailError.'</span>'; ?></span>
                    <br />
                    <b> ☢ Votre évenement est donc... <br /><br />
                    <input id="prive_public" type="radio" class="point" name="privé_public">
                    <option class="point2" value="privé">Privé</option><br />
                    <input  type="radio" class="point" name="privé_public">
                    <option class="point3" value="public">Public</option>
                    <br /><br />
                    <b> ➱ Lieu de l'activité </b><br />
                    <input id="lieu" type="text" class="activité" name="lieu" placeholder="Lieu de l'activité">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$pass2Error.'</span>'; ?></span>
                    <br />
                    <b> 🗓 Date de l'activité </b><br />
                    <label for="start"></label>
                    <input id="date" type="date" id="start" name="trip" class="date" value="2018-09-12" min="2018-01-01" max="2025-12-31" />
                    <br /><br />
                    <b> ⌚ Heure de l'activité </b><br />
                    <input id="heure" type="text" class="activité" name="heure" placeholder="Heure de l'activité">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$nameError.'</span>'; ?></span>
                    <br />
                    <b> 👥 Nombre de personnes </b><br />
                    <input id="nombre_pers" type="text" class="activité" name="nombre_max" placeholder="Nombre de personnes">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$nameError.'</span>'; ?></span>
                    <br />
                    <br />
                    <br />
                    <button id="btn-signup" type="submit" class="envoyer" name="btn-signup"><b>Valider ✔</b></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact section edn -->
<!-- Footer section start -->
<div class="footer">
    <p>&copy; 2018 Copyrights | PlansCampus</p>
</div>
<!-- Footer section end -->
<!-- ScrollUp button start -->
<div class="scrollup">
    <a href="#">
        <i class="icon-up-open"></i>
    </a>
</div>
<!-- ScrollUp button end -->
<!-- Include javascript -->
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/jquery.cslider.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.js"></script>
<script type="text/javascript" src="js/jquery.inview.js"></script>

<!-- Load google maps api and call initializeMap function defined in app.js -->
<script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
<!-- css3-mediaqueries.js for IE8 or older -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
