<?php include("includes/header.php"); ?>
<?php
ob_start();
session_start();
require_once 'BDD.php';

if( !isset($_SESSION['user']) ) {
    header("Location: login.php");
    exit;
}
$query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE id_uti=".$_SESSION['user']);
$query->execute();
$userinfo = $query->fetch();

if (isset($_POST['btn-promo'])) {
  $promo = trim($_POST['promo']);
  $query = $bdd->prepare("UPDATE UTILISATEUR SET promo=$promo WHERE id_uti=".$_SESSION['user'])

    ?>


        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Modification de la promo</h1>
                        <p>Vous pouvez modifier votre promo ici: &nbsp;</p>
                    </div>
                </div>
            </div>



			<div class="details">
               <form action="" method="post">
                <div class="campus">

                <strong>Promo:</strong>
                <select name="promo">
                  <option value="B1">Bachelor 1</option>
                  <option value="B2">Bachelor 2</option>
                  <option value="B3">Bachelor 3</option>
                  <option value="I4">I4</option>
                  <option value="I5">I5</option>
              </select>
                </div>
                <input type="submit" name="btn-promo" value="Valider">
                </form>

            </div>
        </div>
        <div class="container">
            <center><div class="rendez vous">

                    <div class="details">
                        <form action="" method="post">
                            <div class="campus">

                                <strong>Promo:</strong><select>
                                    <option value="1">1ère année</option>
                                    <option value="2">2ème année</option>
                                    <option value="3">3ème année</option>
                                    <option value="4">4ème année</option>
                                    <option value="5">5ème année</option>
                                    <option value="6">6ème année</option>

                                </select>
                                <input type="submit" name="btn-school" value="Valider">
                            </div>
                        </form>
                    </div>


                    <!-- Footer section end -->

                    <!-- ScrollUp button start -->
                    <div class="scrollup">
                        <a href="#">
                            <i class="icon-up-open"></i>
                        </a>
                    </div>
                    <!-- ScrollUp button end -->
                    <!-- Include javascript -->
                    <script src="js/jquery.js"></script>
                    <script type="text/javascript" src="js/jquery.mixitup.js"></script>
                    <script type="text/javascript" src="js/bootstrap.js"></script>
                    <script type="text/javascript" src="js/modernizr.custom.js"></script>
                    <script type="text/javascript" src="js/jquery.bxslider.js"></script>
                    <script type="text/javascript" src="js/jquery.cslider.js"></script>
                    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
                    <script type="text/javascript" src="js/jquery.inview.js"></script>
                    <!-- Load google maps api and call initializeMap function defined in app.js -->
                    <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
                    <!-- css3-mediaqueries.js for IE8 or older -->
                    <!--[if lt IE 9]>
                    <script src="js/respond.min.js"></script>
                    <![endif]-->
                    <script type="text/javascript" src="js/app.js"></script>
                    </body>
                    </html>