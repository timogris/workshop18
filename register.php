<!DOCTYPE html>
<?php
ob_start();
session_start();
if( isset($_SESSION['user'])!="" ){
    header("Location: profil.php");
}
include_once 'BDD.php';

$error = false;

if ( isset($_POST['btn-signup']) ) {

    $name = trim($_POST['name']);
    $name = strip_tags($name);
    $name = htmlspecialchars($name);

    $fname = trim($_POST['fname']);
    $fname = strip_tags($fname);
    $fname = htmlspecialchars($fname);

    $email = trim($_POST['email']);
    $email = strip_tags($email);
    $email = htmlspecialchars($email);

    $pass1 = trim($_POST['pass1']);
    $pass1 = strip_tags($pass1);
    $pass1 = htmlspecialchars($pass1);

    $pass2 = trim($_POST['pass2']);
    $pass2 = strip_tags($pass2);
    $pass2 = htmlspecialchars($pass2);


    // basic name validation
    if (empty($name)) {
        $error = true;
        $nameError = "Veuillez entrer votre nom.";
    }

    if (empty($fname)) {
        $error = true;
        $fnameError = "Veuillez entrer votre prénom.";
    }

    //basic email validation
    if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
        $error = true;
        $emailError = "Veuillez entrer une adresse mail valide.";
    } else {
        // check email exist or not
        $query = $bdd->prepare("SELECT email FROM UTILISATEUR WHERE email= ?");
        $query->execute(array($email));
        $count = $query->rowCount();
        if($count!=0){
            $error = true;
            $emailError = "Cette adresse mail est déjà utilisée.";
        }
    }
    // password validation
    if (empty($pass1)){
        $error = true;
        $pass1Error = "Veuillez entrer un mot de passe.";
    } else if(strlen($pass1) < 6) {
        $error = true;
        $pass1Error = "Votre mot de passe doit contenir au minimum 6 caractères.";
    }

    if (empty($pass2)){
        $error = true;
        $pass2Error = "Les 2 mots de passe doivent correspondre.";
    }

    if($pass1 != $pass2) {
        $error = true;
        $pass1Error = "Les 2 mots de passe de sont pas identiques";
    }

    // password encrypt using SHA256();
    $password = password_hash($pass1, PASSWORD_BCRYPT);

    $tokcount = 1;
    while ($tokcount != 0) {
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $query_cle = $bdd->prepare("SELECT verif_cle FROM UTILISATEUR WHERE verif_cle= ?");
        $query_cle->execute(array($token));
        $tokcount = $query_cle->rowCount();
    }

    // if there's no error, continue to signup
    if( !$error ) {

        $query_add = $bdd->prepare("INSERT INTO UTILISATEUR(id_uti,nom,prenom,email,mdp,verif_cle) VALUES(?,?,?,?,?,?)");
        $query_add->execute(array(NULL, $name, $fname, $email, $password, $token));

        if ($query_add) {

            $to = $email;
            $objet = "Verification de votre compte";
            $headers = 'MIME-Version: 1.0'."\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
            $headers .= 'From: "PlansCampus"<noreply@planscampus.fr>'."\n";
            $headers .= 'Reply-To: noreply@planscampus.fr'."\n";
            $message = file_get_contents("mail.html");
            $message = str_replace('{PRENOM}', $fname, $message);
            $message = str_replace('{NOM}', $name, $message);
            $message = str_replace('{MAIL}', $email, $message);
            $message = str_replace('{TOKEN}', $token, $message);

            mail($to, utf8_encode($objet), utf8_encode($message), $headers);

            $query_mail = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE email=?");
            $query_mail->execute(array($email));

            $_SESSION['user'] = $row['id_uti'];

            $errTyp = "Bravo";
            $errMSG = "Vous êtes désormais enregistré, un email de confirmation vous a été envoyé";

        } else {
            $errTyp = "Attention";
            $errMSG = "Une erreur s'est produite, réessayez plus tard. Si le problème persiste, contactez l'équipe";
        }

    }


}
?>

<html lang="en">

<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <!-- Load Roboto font -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Load css styles -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="css/register.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/pluton.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>

<body>

<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <a href="#" class="brand">
                <img src="images/logo.png" width="120" height="40" alt="Logo" />
                <!-- This is website logo -->

            </a>
            <!-- Navigation button, visible on small resolution -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <i class="icon-menu"></i>
            </button>
            <!-- Main navigation -->
            <div class="nav-collapse collapse pull-right">
                <ul class="nav" id="top-navigation">
                    <li class="active"><a href="index.php">Accueil</a></li>
                    <li class="active"><a href="search.php">Recherche</a></li>
                    <li class="active"><a href="appointment.php">Rendez-vous</a></li>
                    <li class="active"><a href="login.php">Connexion</a></li>
                </ul>
            </div>
            <!-- End main navigation -->
        </div>
    </div>
</div>

<!-- Contact section start -->
<div id="contact" class="contact">
    <div class="section secondary-section">
        <div class="container">
            <div class="title">
                <h1>Inscription</h1>
                <p>Créez un compte ShareBien. Vous avez déjà un compte ? <a href="login.php" class="conninsc">Connectez-vous ici.</a></p>
            </div>

            <?php
            if ($errMSG) {
                if ($errTyp == "Bravo") {
                    echo '<center><p style="color: green">'.$errMSG.'</p></center>';
                } elseif ($errTyp == "Attention") {
                    echo '<center><p style="color: red">'.$errMSG.'</p></center>';
                }
            }
            ?>


        </div>
        <div class="container">
            <div class="inscription">
                <form method="post" action="register.php" autocomplete="off">
                    <b>Prénom :</b><br />
                    <input type="text" class="inscription" name="fname" placeholder="Votre prénom">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$fnameError.'</span>'; ?></span>
                    <br />
                    <b>Nom :</b><br />
                    <input type="text" class="inscription" name="name" placeholder="Votre nom">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$nameError.'</span>'; ?></span>
                    <br />
                    <b>Âge :</b><br />
                    <input type="text" class="inscription" name="yo" placeholder="Votre âge">
                    <br />
                    <b>Campus: </b><br />
                    <input type="text" class="inscription" name="campus" placeholder="Votre campus">
                    <br />
                    <b>École: </b><br />
                    <input type="text" class="inscription" name="school" placeholder="Votre école">
                    <br />
                    <b>Promo: </b><br />
                    <input type="text" class="inscription" name="promo" placeholder="Votre promo">
                    <br />
                    <b>Email :</b><br />
                    <input type="email" class="inscription" name="email" placeholder="Votre Email">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$emailError.'</span>'; ?></span>
                    <br />
                    <b>Mot de passe :</b><br />
                    <input type="password" class="inscription" name="pass1" placeholder="Votre mot de passe">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$pass1Error.'</span>'; ?></span>
                    <br />
                    <b>Confirmez votre mot de passe :</b><br />
                    <input type="password" class="inscription" name="pass2" placeholder="Votre mot de passe">
                    <span class="text-danger"><?php echo '<br /><span style="color: red">'.$pass2Error.'</span>'; ?></span>
                    <br />
                    <br />
                    <button type="submit" class="envoyer" name="btn-signup"><b>Envoyer</b></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact section edn -->
<!-- Footer section start -->
<div class="footer">
    <p>&copy; 2018 Copyrights | PlansCampus</p>
</div>
<!-- Footer section end -->
<!-- ScrollUp button start -->
<div class="scrollup">
    <a href="#">
        <i class="icon-up-open"></i>
    </a>
</div>
<!-- ScrollUp button end -->
<!-- Include javascript -->
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/jquery.cslider.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.js"></script>
<script type="text/javascript" src="js/jquery.inview.js"></script>
<!-- Load google maps api and call initializeMap function defined in app.js -->
<script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
<!-- css3-mediaqueries.js for IE8 or older -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
