<?php include("includes/header.php"); ?>
<?php

ob_start();
session_start();
require_once 'BDD.php';

if( !isset($_SESSION['user']) ) {
    header("Location: login.php");
    exit;
}

$query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE id_uti=".$_SESSION['user']);
$query->execute();
$userinfo = $query->fetch();

if (isset($_POST['btn-mdp'])) {

    $error = false;

    $pass = trim($_POST['ancienmdp']);
    $pass = strip_tags($pass);
    $pass = htmlspecialchars($pass);

    $pass1 = trim($_POST['nouveaumdp']);
    $pass1 = strip_tags($pass1);
    $pass1 = htmlspecialchars($pass1);

    $pass2 = trim($_POST['confmdp']);
    $pass2 = strip_tags($pass2);
    $pass2 = htmlspecialchars($pass2);

    if(empty($pass)) {
        $error = true;
        $passError = "Veuillez entrer votre mot de passe actuel";
    }

    if(!password_verify($pass, $userinfo['mdp'])) {
        $error = true;
        $passError = "Le mot de passe est incorrect";
    }

    // password validation
    if (empty($pass1)){
        $error = true;
        $pass1Error = "Veuillez entrer un mot de passe.";
    } else if(strlen($pass1) < 6) {
        $error = true;
        $pass1Error = "Votre mot de passe doit contenir au minimum 6 caractères.";
    }

    if (empty($pass2)){
        $error = true;
        $pass2Error = "Les 2 mots de passe doivent correspondre.";
    }

    if($pass1 != $pass2) {
        $error = true;
        $pass1Error = "Les 2 mots de passe de sont pas identiques";
    }

    if(!$error) {

        // password encrypt using BCRYPT
        $password = password_hash($pass1, PASSWORD_BCRYPT);

        $query_mdp = $bdd->prepare("UPDATE UTILISATEUR SET mdp=".$password." WHERE id_uti=".$_SESSION['user']);
        $exec = $query_mdp->execute();

        if ($exec) {
            $error = true;
            $errTyp = "Bravo";
            $errMSG = "Votre mot de passe a bien été modifié";
            ?>
            <script type="text/javascript">
                redirectTime = "2000";
                redirectURL = "profil.php";
                setTimeout("location.href = redirectURL;",redirectTime);
            </script>
            <?php
        } else {
            $error = true;
            $errTyp = "Attention";
            $errMSG = "Un problème est survenu, contacter l'administrateur";
        }
    }
}
?>

        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Modification du mot de passe</h1>
                        <p>Vous pouvez modifier votre mot de passe ici: &nbsp;</p>
                    </div>
                </div>
                <div class="container">
                    <center><div class="rendez vous">

			<div class="details">
               <form action="" method="post">
                <div class="verifs">


                <strong>Mot de passe:</strong> <input type="password" name="ancienmdp" value="" placeholder="Ancien mot de passe">
                <br /><br /><br />                                 <input type="password" name="nouveaumdp" value="" placaeholder="Nouveau mot de passe">
                <br /><br /><br />                                 <input type="password" name="confmdp" value="" placeholder="Confirmer mot de passe">
                    <?php
                    if ($error) {
                        if ($errTyp == "Bravo") {
                            echo '<center><p style="color: green">'.$errMSG.'</p></center>';
                        } elseif ($errTyp == "Attention") {
                            echo '<center><p style="color: red">'.$errMSG.'</p></center>';
                        }
                    }
                    ?>

                <strong>Ancien mot de passe:</strong>
                    <input type="password" name="ancienmdp" value="" placeholder="Ancien mot de passe">
                    <?php if(isset($passError)){
                      echo '<center><p style="color: red">'.$passError.'</p></center>';
                    }
                    ?>
                <br /><br /><br />
                <strong>Nouveau mot de passe:</strong>
                    <input type="password" name="nouveaumdp" value="" placeholder="Nouveau mot de passe">
                    <?php if(isset($pass1Error)){
                      echo '<center><p style="color: red">'.$pass1Error.'</p></center>';
                    }
                    ?>
                <br /><br /><br />
                <strong>Confirmer nouveau mot de passe:</strong>
                    <input type="password" name="confmdp" value="" placeholder="Confirmer nouveau mot de passe">
                    <?php if(isset($pass2Error)){
                      echo '<center><p style="color: red">'.$pass2Error.'</p></center>';
                    }
                    ?>



                </div>
                                            <input type="submit" name="btn-mdp" value="Valider">
                </form>
            </div>
                        </div></center></div></div></div>



        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
