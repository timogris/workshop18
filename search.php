<?php include("includes/header.php"); ?>

<?php
require_once 'BDD.php';


    $query = $bdd->prepare("SELECT * FROM ACTIVITE");
    $query->execute();

    $query2 = $bdd->prepare("SELECT * FROM COVOITURAGE");
    $query2->execute();
?>
        <!-- Portfolio section start -->
        <div class="section secondary-section tout">
            <div class="container">
                <div class="title">
                    <h1>Recherche des activités :</h1>
                    <input type="text" class="rechercherbarre" placeholder="Rechercher ..." title="Tapez une recherche">
		                <button type="submit" class="rechercher">Rechercher</button>
		                <br />
                		<form>
                		<input type="radio" href="#" data-filter="co-voiturage" tabindex="-1" name="add"> Co-voiturage&nbsp; &nbsp;
                		<input type="radio" href="#" data-filter="divertissement" tabindex="-1" name="add"> Sortie divertissante&nbsp; &nbsp;
                    <input type="radio" href="#" data-filter="culture" tabindex="-1" name="add"> Sortie culturelle&nbsp; &nbsp;
                		<input type="radio" href="#" data-filter="restauration" tabindex="-1" name="add"> Restauration&nbsp; &nbsp;
                		</form>
                </div>


                </div>
                <!-- Start details for portfolio project 1 -->

                <!-- events -->

              <div class="touslesevents">


                  <?php
                    while ($row = $query->fetch()) {
                        $req = $bdd->prepare("SELECT nom, prenom, photo FROM UTILISATEUR WHERE id_uti=".$row['id_uti']);
                        $req->execute();
                        $res = $req->fetch();
                        ?>

                        <div class="events_sorties">
                            <img class="photo_event" src="<?php echo 'img_profil/'.$res['photo'].''?>" alt="">
                            <div class="infos">
                                <p class="info"><?php echo $res['nom'];?>&nbsp; <?php echo $res['prenom'];?></p><br/>
                                <p class="info"><?php echo $row['nom'];?></p><br/>
                                <p class="info"><?php echo $row['lieu'];?></p><br/>
                                <p class="info"><?php echo $row['date'];?>&nbsp;&nbsp; <?php echo $row['heure'];?></p><br/>
                           </div>
                            <div class="description">
                                <p><?php echo $row['description'];?></p>
                            </div>
                            <a href="eventsorties.php" class="plusinfo">+ d'info</a>
                        </div>
                  <?php
                    }
                  ?>


                  <?php
                  while ($row2 = $query2->fetch()) {
                      $req = $bdd->prepare("SELECT nom, prenom, photo FROM UTILISATEUR WHERE id_uti=".$row2['id_uti']);
                      $req->execute();
                      $res = $req->fetch();
                      ?>

                      <div class="events_sorties">
                          <img class="photo_event" src="<?php echo 'img_profil/'.$res['photo'].''?>" alt="">
                          <div class="infos">
                              <p class="info"><?php echo $res['nom'];?>&nbsp; <?php echo $res['prenom'];?></p> <br/>
                              <p class="info">Covoiturage</p><br/>
                              <p class="info"><?php echo $row2['prix'];?> €</p><br/>
                              <p class="info"><?php echo $row2['date_depart'];?></p> <br/>
                              <p class="info"><?php echo $row2['depart'];?>&nbsp; &nbsp;<?php echo $row2['heure_depart'];?></p> <br/>
                              <p class="info"><?php echo $row2['arrivee'];?>&nbsp; &nbsp;<?php echo $row2['heure_arrivee'];?></p><br/>
                          </div>
                          <div class="description">
                              <p><?php echo $row2['descrption'];?></p>
                          </div>
                          <a href="eventcovoit.php" class="plusinfo">+ d'info</a>
                      </div>
                      <?php
                  }
                  ?>


                </div>
              </div>

    </body>
</html>
