<?php include("includes/header.php"); ?>

        <!-- Start home section -->
        <div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
                <!--<div class="triangle"></div> -->
                <!-- mask elemet use for masking background image -->
                <div class="mask"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                        <h2 class="fittext2">Sortie ski au 7 Laux</h2>
                        <h4> 5 places disponibles </h4>
 			<p><a href="eventactivite.php">Plus d'infos ici...</a></p>
                        <div class="da-img">
                            <img src="images/Neige.png" alt="image01" width="320">
                        </div>
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
                    <div class="da-slide">
                        <h2>Co-voiturage pour Paris</h2>
                        <h4>3 places disponibles</h4>
                        <p><a href="eventactivite.php">Plus d'infos ici...</a></p>
                        <div class="da-img">
                            <img src="images/Paris.png" width="320" alt="image02">
                        </div>
                    </div>
                    <!-- End second slide -->
                    <!-- Start third slide -->
                    <div class="da-slide">
                        <h2>After Work jeudi 20 septembre</h2>
                        <h4>Rendez-vous au bar Paradise à 20h</h4>
                        <p><a href="eventactivite.php">Plus d'infos ici... </a></p>
                        <div class="da-img">
                            <img src="images/Bar.png" width="320" alt="image03">
                        </div>
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Plans Campus est une plateforme de partage de bons plans et d'événements organisés par les étudiants et pour les étudiants du campus HEP<br/> </h1>
                    <!-- Section's title goes here -->

                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                            </div>
                            <h3>Très rapide</h3>
                            <p>Publier ou s'inscrire sur des annonces ou des événements en vous connectant avec votre adresse d'école</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                            </div>
                            <h3>Tout est gratuit</h3>
                            <p>Accédez à tous les évènements du campus en quelques clics seulement</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">

                            </div>
                            <h3>Pour la suite</h3>

                            <p>Vous avez une idée de sortie ou un co-voiturage à partager ? Publiez l'événement dès maintenant <a href="add.php"> ici </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service section end -->

        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
