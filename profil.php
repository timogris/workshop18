<?php include("includes/header.php"); ?>

    <?php
    ob_start();
    require_once 'BDD.php';

    if( !isset($_SESSION['user']) ) {
        header("Location: login.php");
        exit;
    }

    $query = $bdd->prepare("SELECT * FROM UTILISATEUR WHERE id_uti=".$_SESSION["user"]);
    $query->execute();
    $userinfo = $query->fetch();

?>
     <!-- About us section start -->
        <div class="section primary-section" id="about">
            <div class="container">
                <div class="title">
                    <h1>Mon profil</h1>
                    <p>Cette page réunie toutes vos informations PlansCampus</p>
                </div>
                <div class="row-fluid team">
                    <div class="span4" id="first-person" style="margin-right: 25px">
                        <div class="thumbnail" style="height: 450px">git
                            <h3><?php echo '<b style="color: white">'.$userinfo['nom'].'&nbsp;&nbsp;'.$userinfo['prenom'].'</b>'?></h3>

                            <div class="mask">
                                <h2>Image de profil</h2>
                                <p><a href="changeimg.php" style="color: white">Changer mon image</a></p>
                            </div>
                        </div>
                    </div>

						<div>
							<h2>Mes informations personnelles</h2><hr />
							<p><b style="font-size: 20px; color: grey">EMAIL: &nbsp;&nbsp;</b><?php echo $userinfo['email'] ?></p>
                            <p><b style="font-size: 20px; color: grey">ECOLE: &nbsp;&nbsp;</b><?php echo $userinfo['ecole'] ?></p>
                            <p><b style="font-size: 20px; color: grey">PROMO: &nbsp;&nbsp;</b><?php echo $userinfo['promo'] ?></p>

							<a href="changemail.php"><button>Changer mon Email</button></a>
							<a href="changemdp.php"><button>Changer mon Mot De Passe</button></a>
							<a href="changepromo.php"><button>Changer ma Promo</button></a>
							<a href="changeecole.php"><button>Changer mon Campus</button></a>
						</div>

                </div>

            </div>
        </div>
 <!-- Footer section start -->
        <div class="footer">
            <p>&copy; 2018 Copyrights | PlansCampus</p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
